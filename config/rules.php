<?php
declare(strict_types=1);

use PhpCsFixerCustomFixers\Fixer\MultilineCommentOpeningClosingAloneFixer;
use PhpCsFixerCustomFixers\Fixer\MultilinePromotedPropertiesFixer;
use PhpCsFixerCustomFixers\Fixer\NoDoctrineMigrationsGeneratedCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoDuplicatedArrayKeyFixer;
use PhpCsFixerCustomFixers\Fixer\NoDuplicatedImportsFixer;
use PhpCsFixerCustomFixers\Fixer\NoPhpStormGeneratedCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoSuperfluousConcatenationFixer;
use PhpCsFixerCustomFixers\Fixer\NoTrailingCommaInSinglelineFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessDoctrineRepositoryCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessParenthesisFixer;
use PhpCsFixerCustomFixers\Fixer\NumericLiteralSeparatorFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocNoIncorrectVarAnnotationFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocParamOrderFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocParamTypeFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocSelfAccessorFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocSingleLineVarFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocTypesCommaSpacesFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocTypesTrimFixer;
use PhpCsFixerCustomFixers\Fixer\PromotedConstructorPropertyFixer;
use PhpCsFixerCustomFixers\Fixer\StringableInterfaceFixer;

return [
	'PJ/braces' => true,
	MultilineCommentOpeningClosingAloneFixer::name() => true,
	MultilinePromotedPropertiesFixer::name() => true,
	NoDoctrineMigrationsGeneratedCommentFixer::name() => true,
	NoDuplicatedArrayKeyFixer::name() => true,
	NoDuplicatedImportsFixer::name() => true,
	NoPhpStormGeneratedCommentFixer::name() => true,
	NoSuperfluousConcatenationFixer::name() => true,
	NoUselessCommentFixer::name() => true,
	NoUselessDoctrineRepositoryCommentFixer::name() => true,
	NoUselessParenthesisFixer::name() => true,
	NumericLiteralSeparatorFixer::name() => true,
	PhpdocNoIncorrectVarAnnotationFixer::name() => true,
	PhpdocParamOrderFixer::name() => true,
	PhpdocParamTypeFixer::name() => true,
	PhpdocSelfAccessorFixer::name() => true,
	PhpdocSingleLineVarFixer::name() => true,
	PhpdocTypesTrimFixer::name() => true,
	PromotedConstructorPropertyFixer::name() => true,
	StringableInterfaceFixer::name() => true,
	NoTrailingCommaInSinglelineFixer::name() => true,
	PhpdocTypesCommaSpacesFixer::name() => true,

	'align_multiline_comment' => [
		'comment_type' => 'phpdocs_like',
	],

	'array_indentation' => true,
	'array_push' => true,
	'array_syntax' => true,
	'assign_null_coalescing_to_coalesce_equal' => true,
	'binary_operator_spaces' => true,
	'blank_line_after_namespace' => true,

	'blank_line_before_statement' => [
		'statements' => [
			'case',
			'default',
			'do',
			'for',
			'foreach',
			'if',
			'switch',
			'try',
			'while',
			'phpdoc',
		],
	],

	'braces' => [
		'position_after_control_structures' => 'next',
	],

	'cast_spaces' => [
		'space' => 'none',
	],

	'class_attributes_separation' => true,

	'class_definition' => [
		'space_before_parenthesis' => true,
	],

	'clean_namespace' => true,
	'combine_consecutive_issets' => true,
	'combine_consecutive_unsets' => true,
	'combine_nested_dirname' => true,
	'compact_nullable_typehint' => true,
	'concat_space' => true,
	'constant_case' => true,

	'control_structure_continuation_position' => [
		'position' => 'next_line',
	],

	'declare_equal_normalize' => true,
	'declare_parentheses' => true,
	'declare_strict_types' => true,
	'dir_constant' => true,

	'echo_tag_syntax' => [
		'format' => 'short',
		'shorten_simple_statements_only' => false,
	],

	'elseif' => true,
	'empty_loop_body' => true,
	'empty_loop_condition' => true,
	'encoding' => true,
	'ereg_to_preg' => true,

	'error_suppression' => [
		'mute_deprecation_error' => false,
		'noise_remaining_usages' => true,
	],

	'fopen_flag_order' => true,
	'fopen_flags' => true,
	'full_opening_tag' => true,
	'fully_qualified_strict_types' => true,

	'function_declaration' => [
		'closure_function_spacing' => 'none',
	],

	'function_to_constant' => true,
	'function_typehint_space' => true,
	'general_phpdoc_tag_rename' => true,

	'global_namespace_import' => [
		'import_constants' => true,
		'import_functions' => true,
	],

	'implode_call' => true,
	'include' => true,

	'increment_style' => [
		'style' => 'post',
	],

	'indentation_type' => true,
	'integer_literal_case' => true,
	'is_null' => true,
	'lambda_not_used_import' => true,
	'line_ending' => true,
	'linebreak_after_opening_tag' => true,
	'list_syntax' => true,
	'logical_operators' => true,
	'lowercase_cast' => true,
	'lowercase_keywords' => true,
	'lowercase_static_reference' => true,
	'magic_constant_casing' => true,
	'magic_method_casing' => true,

	'method_argument_space' => [
		'on_multiline' => 'ensure_fully_multiline',
	],

	'modernize_types_casting' => true,
	'multiline_whitespace_before_semicolons' => true,
	'native_function_casing' => true,
	'native_function_type_declaration_casing' => true,
	'new_with_braces' => true,

	'no_alias_functions' => [
		'sets' => [
			'@all',
		],
	],

	'no_alias_language_construct_call' => true,

	'no_alternative_syntax' => [
		'fix_non_monolithic_code' => false,
	],

	'no_blank_lines_after_class_opening' => true,
	'no_blank_lines_after_phpdoc' => true,
	'no_closing_tag' => true,
	'no_empty_comment' => true,
	'no_empty_phpdoc' => true,
	'no_empty_statement' => true,

	'no_extra_blank_lines' => [
		'tokens' => [
			'curly_brace_block',
			'extra',
			'parenthesis_brace_block',
			'return',
			'square_brace_block',
			'throw',
			'use',
		],
	],

	'no_leading_import_slash' => true,
	'no_leading_namespace_whitespace' => true,
	'no_mixed_echo_print' => true,
	'no_multiline_whitespace_around_double_arrow' => true,
	'no_php4_constructor' => true,
	'no_short_bool_cast' => true,
	'no_singleline_whitespace_before_semicolons' => true,
	'no_space_around_double_colon' => true,
	'no_spaces_after_function_name' => true,
	'no_spaces_around_offset' => true,
	'no_spaces_inside_parenthesis' => true,
	'no_superfluous_elseif' => true,
	'no_superfluous_phpdoc_tags' => true,
	'no_trailing_comma_in_list_call' => true,
	'no_trailing_comma_in_singleline_array' => true,
	'no_trailing_whitespace' => true,
	'no_trailing_whitespace_in_comment' => true,

	'no_unneeded_control_parentheses' => [
		'statements' => [
			'break',
			'clone',
			'continue',
			'echo_print',
			'return',
			'switch_case',
			'yield',
			'yield_from',
		],
	],

	'no_unneeded_curly_braces' => [
		'namespaces' => true,
	],

	'no_unneeded_final_method' => true,
	'no_unused_imports' => true,
	'no_useless_else' => true,
	'no_useless_return' => true,
	'no_useless_sprintf' => true,
	'no_whitespace_before_comma_in_array' => true,
	'no_whitespace_in_blank_line' => true,
	'normalize_index_brace' => true,
	'nullable_type_declaration_for_default_null_value' => true,
	'object_operator_without_whitespace' => true,

	'operator_linebreak' => [
		'only_booleans' => true,
		'position' => 'end',
	],

	'ordered_class_elements' => [
		'order' => [
			'use_trait',
			'constant_public',
			'property_public_readonly',
			'property_public',
			'property_public_static',
			'constant_protected',
			'property_protected_readonly',
			'property_protected',
			'property_protected_static',
			'constant_private',
			'property_private_readonly',
			'property_private',
			'property_private_static',
			'construct',
			'destruct',
			'magic',
			'phpunit',
			'method_public',
			'method_public_static',
			'method_public_abstract',
			'method_public_abstract_static',
			'method_protected',
			'method_protected_static',
			'method_protected_abstract',
			'method_protected_abstract_static',
			'method_private',
			'method_private_static',
		],
	],

	'ordered_imports' => [
		'imports_order' => [
			'const',
			'class',
			'function',
		],
	],

	'php_unit_construct' => true,
	'php_unit_method_casing' => true,
	'php_unit_mock_short_will_return' => true,

	'phpdoc_align' => [
		'align' => 'left',

		'tags' => [
			'method',
			'param',
			'property',
			'property-read',
			'property-write',
			'return',
			'throws',
			'type',
			'var',
		],
	],

	'phpdoc_indent' => true,
	'phpdoc_inline_tag_normalizer' => true,
	'phpdoc_no_access' => true,
	'phpdoc_no_alias_tag' => true,
	'phpdoc_no_empty_return' => true,
	'phpdoc_no_package' => true,
	'phpdoc_no_useless_inheritdoc' => true,
	'phpdoc_order' => true,
	'phpdoc_return_self_reference' => true,
	'phpdoc_scalar' => true,
	'phpdoc_separation' => true,
	'phpdoc_single_line_var_spacing' => true,
	'phpdoc_summary' => true,
	'phpdoc_tag_casing' => true,

	'phpdoc_tag_type' => [
		'tags' => [
			'api' => 'annotation',
			'author' => 'annotation',
			'copyright' => 'annotation',
			'deprecated' => 'annotation',
			'example' => 'annotation',
			'global' => 'annotation',
			'inheritDoc' => 'inline',
			'internal' => 'annotation',
			'license' => 'annotation',
			'method' => 'annotation',
			'package' => 'annotation',
			'param' => 'annotation',
			'property' => 'annotation',
			'return' => 'annotation',
			'see' => 'annotation',
			'since' => 'annotation',
			'throws' => 'annotation',
			'todo' => 'annotation',
			'uses' => 'annotation',
			'var' => 'annotation',
			'version' => 'annotation',
		],
	],

	'phpdoc_to_comment' => true,
	'phpdoc_trim' => true,
	'phpdoc_trim_consecutive_blank_line_separation' => true,
	'phpdoc_types' => true,

	'phpdoc_types_order' => [
		'null_adjustment' => 'always_last',
	],

	'phpdoc_var_annotation_correct_order' => true,
	'phpdoc_var_without_name' => true,
	'pow_to_exponentiation' => true,
	'protected_to_private' => true,
	'return_type_declaration' => true,
	'self_static_accessor' => true,
	'semicolon_after_instruction' => true,
	'short_scalar_cast' => true,
	'simplified_if_return' => true,
	'simplified_null_return' => true,
	'single_blank_line_before_namespace' => true,
	'single_class_element_per_statement' => true,
	'single_import_per_statement' => true,
	'single_line_after_imports' => true,
	'single_line_comment_style' => true,
	'single_quote' => true,

	'single_space_after_construct' => [
		'constructs' => [
			'abstract',
			'as',
			'attribute',
			'break',
			'case',
			'class',
			'clone',
			'comment',
			'const',
			'const_import',
			'continue',
			'echo',
			'enum',
			'extends',
			'final',
			'finally',
			'function_import',
			'global',
			'goto',
			'implements',
			'include',
			'include_once',
			'instanceof',
			'insteadof',
			'interface',
			'named_argument',
			'namespace',
			'new',
			'open_tag_with_echo',
			'php_doc',
			'php_open',
			'print',
			'private',
			'protected',
			'public',
			'readonly',
			'require',
			'require_once',
			'return',
			'static',
			'throw',
			'trait',
			'use',
			'use_lambda',
			'use_trait',
			'var',
			'yield',
			'yield_from',
		],
	],

	'single_trait_insert_per_statement' => true,
	'space_after_semicolon' => true,
	'standardize_increment' => true,
	'standardize_not_equals' => true,
	'static_lambda' => true,
	'switch_case_semicolon_to_colon' => true,
	'switch_case_space' => true,
	'switch_continue_to_break' => true,
	'ternary_operator_spaces' => true,
	'ternary_to_elvis_operator' => true,
	'ternary_to_null_coalescing' => true,

	'trailing_comma_in_multiline' => [
		'elements' => [
			'arguments',
			'arrays',
			'parameters',
		],
	],

	'trim_array_spaces' => true,
	'types_spaces' => true,
	'unary_operator_spaces' => true,
	'use_arrow_functions' => true,
	'visibility_required' => true,
	'void_return' => true,
	'whitespace_after_comma_in_array' => true,

	'yoda_style' => [
		'always_move_variable' => true,
	],

	'class_reference_name_casing' => true,
	'get_class_to_class_keyword' => true,
	'no_trailing_comma_in_singleline_function_call' => true,
	'no_unneeded_import_alias' => true,
];