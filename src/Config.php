<?php
declare(strict_types=1);

namespace PJ\PhpCsFixer;

use PhpCsFixer\Config as BaseConfig;
use PhpCsFixer\ConfigInterface;
use PhpCsFixerCustomFixers\Fixer\MultilineCommentOpeningClosingAloneFixer;
use PhpCsFixerCustomFixers\Fixer\MultilinePromotedPropertiesFixer;
use PhpCsFixerCustomFixers\Fixer\NoDoctrineMigrationsGeneratedCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoDuplicatedArrayKeyFixer;
use PhpCsFixerCustomFixers\Fixer\NoDuplicatedImportsFixer;
use PhpCsFixerCustomFixers\Fixer\NoPhpStormGeneratedCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoSuperfluousConcatenationFixer;
use PhpCsFixerCustomFixers\Fixer\NoTrailingCommaInSinglelineFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessDoctrineRepositoryCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessParenthesisFixer;
use PhpCsFixerCustomFixers\Fixer\NumericLiteralSeparatorFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocNoIncorrectVarAnnotationFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocParamOrderFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocParamTypeFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocSelfAccessorFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocSingleLineVarFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocTypesCommaSpacesFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocTypesTrimFixer;
use PhpCsFixerCustomFixers\Fixer\PromotedConstructorPropertyFixer;
use PhpCsFixerCustomFixers\Fixer\StringableInterfaceFixer;
use PJ\PhpCsFixer\Fixer\Basic\BracesFixer;

final class Config
{
	public static function get(): ConfigInterface
	{
		$config = new BaseConfig();
		$rules = require dirname(__DIR__).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'rules.php';

		$config
			->registerCustomFixers([
				new BracesFixer(),
				new MultilineCommentOpeningClosingAloneFixer(),
				new MultilinePromotedPropertiesFixer(),
				new NoDoctrineMigrationsGeneratedCommentFixer(),
				new NoDuplicatedArrayKeyFixer(),
				new NoDuplicatedImportsFixer(),
				new NoPhpStormGeneratedCommentFixer(),
				new NoSuperfluousConcatenationFixer(),
				new NoUselessCommentFixer(),
				new NoUselessDoctrineRepositoryCommentFixer(),
				new NoUselessParenthesisFixer(),
				new NumericLiteralSeparatorFixer(),
				new PhpdocNoIncorrectVarAnnotationFixer(),
				new PhpdocParamOrderFixer(),
				new PhpdocParamTypeFixer(),
				new PhpdocSelfAccessorFixer(),
				new PhpdocSingleLineVarFixer(),
				new PhpdocTypesTrimFixer(),
				new PromotedConstructorPropertyFixer(),
				new StringableInterfaceFixer(),
				new NoTrailingCommaInSinglelineFixer(),
				new PhpdocTypesCommaSpacesFixer(),
			])
			->setLineEnding("\n")
			->setIndent("\t")
			->setRiskyAllowed(true)
			->setRules($rules);

		return $config;
	}
}