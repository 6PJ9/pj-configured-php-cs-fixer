<?php
declare(strict_types=1);

namespace PJ\PhpCsFixer\Fixer\Basic;

use PhpCsFixer\Fixer\WhitespacesAwareFixerInterface;
use PhpCsFixer\FixerDefinition\CodeSample;
use PhpCsFixer\FixerDefinition\FixerDefinition;
use PhpCsFixer\FixerDefinition\FixerDefinitionInterface;
use PhpCsFixer\Tokenizer\Token;
use PhpCsFixer\Tokenizer\Tokens;
use PhpCsFixer\WhitespacesFixerConfig;
use SplFileInfo;

final class BracesFixer implements WhitespacesAwareFixerInterface
{
	private const CONTROL_TOKENS = [
		T_ELSEIF,
		T_FOR,
		T_FOREACH,
		T_IF,
		T_WHILE,
		T_CATCH,
		T_SWITCH,
		T_MATCH,
	];

	private WhitespacesFixerConfig $whitespacesConfig;

	public function __construct()
	{
		$this->whitespacesConfig = new WhitespacesFixerConfig('    ', "\n");
	}

	public function isCandidate(Tokens $tokens): bool
	{
		return $tokens->isAnyTokenKindsFound(self::CONTROL_TOKENS);
	}

	public function isRisky(): bool
	{
		return false;
	}

	public function fix(SplFileInfo $file, Tokens $tokens): void
	{
		for($index = $tokens->count() - 1; 0 <= $index; $index--)
		{
			$token = $tokens[$index];

			if($token->isGivenKind(self::CONTROL_TOKENS))
			{
				$tokens->removeTrailingWhitespace($index);
			}

			if($token->isGivenKind([T_ELSE, T_ELSEIF, T_CATCH]))
			{
				$prevNonWhitespace = $tokens->getPrevNonWhitespace($index);

				if($this->shouldAddBlankLine($tokens, $prevNonWhitespace))
				{
					$this->insertBlankLine($tokens, $index);
				}

				$index = $prevNonWhitespace;
			}
		}
	}

	public function getDefinition(): FixerDefinitionInterface
	{
		return new FixerDefinition(
			'Linijka odstępu przed else i elseif. Brak spacji po if, elseif, for itd.',
			[
				new CodeSample(
					'<?php
if (true)
{

}
',
				),

				new CodeSample(
					'<?php
if(true)
{

}
elseif(1 === $x)
{

}
else
{

}
',
				),
			],
		);
	}

	public function getName(): string
	{
		return 'PJ/braces';
	}

	public function getPriority(): int
	{
		return -999;
	}

	public function supports(SplFileInfo $file): bool
	{
		return true;
	}

	public function setWhitespacesConfig(WhitespacesFixerConfig $config): void
	{
		$this->whitespacesConfig = $config;
	}

	private function shouldAddBlankLine(Tokens $tokens, int $prevNonWhitespace): bool
	{
		$prevNonWhitespaceToken = $tokens[$prevNonWhitespace];

		if($prevNonWhitespaceToken->isComment())
		{
			for($j = $prevNonWhitespace - 1; $j >= 0; $j--)
			{
				if(str_contains($tokens[$j]->getContent(), "\n"))
				{
					return false;
				}

				if($tokens[$j]->isWhitespace() || $tokens[$j]->isComment())
				{
					continue;
				}

				return $tokens[$j]->equalsAny([';', '}']);
			}
		}

		return $prevNonWhitespaceToken->equalsAny([';', '}']);
	}

	private function insertBlankLine(Tokens $tokens, int $index): void
	{
		$prevIndex = $index - 1;
		$prevToken = $tokens[$prevIndex];
		$lineEnding = $this->whitespacesConfig->getLineEnding();

		if($prevToken->isWhitespace())
		{
			$newlinesCount = substr_count($prevToken->getContent(), "\n");

			if(0 === $newlinesCount)
			{
				$tokens[$prevIndex] = new Token([T_WHITESPACE, rtrim($prevToken->getContent(), " \t").$lineEnding.$lineEnding]);
			}

			elseif(1 === $newlinesCount)
			{
				$tokens[$prevIndex] = new Token([T_WHITESPACE, $lineEnding.$prevToken->getContent()]);
			}
		}

		else
		{
			$tokens->insertAt($index, new Token([T_WHITESPACE, $lineEnding.$lineEnding]));
		}
	}
}