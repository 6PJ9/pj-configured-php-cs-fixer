# Instalacja
- Wykonać polecenie `composer require --dev pj/configured-php-cs-fixer`.
- W głównym katalogu projektu utworzyć plik `.php-cs-fixer.dist.php` z zawartością (dostosować do projektu):

```php
<?php
declare(strict_types=1);

use PhpCsFixer\Finder;
use PJ\PhpCsFixer\Config;

$finder = Finder::create()
	->in(__DIR__)
	->ignoreUnreadableDirs(true)
	->ignoreVCSIgnored(true)
	->exclude('config/secrets');

return Config::get()
	->setFinder($finder);
```

- Dodać wpis do `composer.json`:

```json
"scripts": {
	"fix-cs": "./vendor/bin/php-cs-fixer fix -v"
}
```

- Dodać plik `.php-cs-fixer.cache` do `.gitignore`.